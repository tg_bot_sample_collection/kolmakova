import asyncio
import logging
from os import getenv
from sys import exit
from aiogram import Bot, Dispatcher, executor, types


bot_token = getenv("BOT_TOKEN")
if not bot_token:
    exit("Error: no token provided")

bot = Bot(token=bot_token, parse_mode=types.ParseMode.HTML)

# Диспетчер для бота
dp = Dispatcher(bot)
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)

# from aiogram import types
@dp.message_handler(commands="start")
async def cmd_start(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Осмотреться", "Еще полежать с закрытыми глазами"]
    keyboard.add(*buttons)
    await message.answer("Dungeon quest")
    await asyncio.sleep(1)
    await message.answer("Вы просыпаетесь, в незнакомой комнате, у вас сильно болит голова")
    await asyncio.sleep(1)
    await message.answer("Что вы хотите сделать?", reply_markup=keyboard)

# from aiogram.dispatcher.filters import Text
@dp.message_handler(lambda message: message.text == "Осмотреться")
async def osmotr(message: types.Message):
    await message.answer("Вы осматриваете комнату", reply_markup=types.ReplyKeyboardRemove())
    await asyncio.sleep(1)
    await message.answer("Вокруг темная , сырая комната , состоящая из заплесневелых холодных камней , в конце комнаты находится дверь , рядом с ней стоит единственный источник света - факел , он стоит на металлической подставке , закрепленной на стене.")
    await asyncio.sleep(1)
    await message.answer("В комнате , неподалеку от вас ,есть небольшой старинный шкафчик , на нем стоит потухшая свеча и лежит пистолет")
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Взять пистолет", "Подойти к двери"]
    keyboard.add(*buttons)
    await message.answer("Что вы хотите сделать дальше?", reply_markup=keyboard)

@dp.message_handler(lambda message: message.text == "Еще полежать с закрытыми глазами")
async def lejat(message: types.Message):
    await message.reply("Так неинтересно! Давай открывай глаза и осматривайся")

@dp.message_handler(lambda message: message.text == "Взять пистолет" or  message.text == "Подойти к двери")
async def pistolet(message: types.Message):
    await message.answer("Вы делаете первый шаг", reply_markup=types.ReplyKeyboardRemove())
    await asyncio.sleep(1)
    await message.answer("О НЕТ !!! КАК ЖЕ Я СРАЗУ НЕ ЗАМЕТИЛ-подумали про себя вы")
    await asyncio.sleep(1)
    await message.answer("на вашу ногу в кандалы , от них идет цепь к стене")
    await asyncio.sleep(1)
    await message.answer("Ну нет уж , так не пойдет , нужно выбираться отсюда!- подумали вы")
    await asyncio.sleep(1)
    await message.answer("Что вы хотите сделать дальше?")
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="попробовать выдернуть цепь из стены", callback_data="value1"))
    keyboard.add(types.InlineKeyboardButton(text="позвать на помощь", callback_data="value2"))
    keyboard.add(types.InlineKeyboardButton(text="осмотреться вокруг и поискать что-нибудь в карманах", callback_data="value3"))
    keyboard.add(types.InlineKeyboardButton(text="цепь не такая и толстая , может попробовать прокусить???", callback_data="value4"))
    await message.answer("Нажмите на кнопку, чтобы выбрать свое дальнейшее действие", reply_markup=keyboard)


@dp.callback_query_handler(text="value1")
async def naj_value1(call: types.CallbackQuery):
    await call.answer(text="Ха-ха , хорошая попытка ,может еще попробуешь???", show_alert=True)
@dp.callback_query_handler(text="value2")
async def naj_value2(call: types.CallbackQuery):
    await call.answer(text="Вы прокричали охриплым голосом *ПОМОГИТЕ!* , но никто не ответил , какая жалость!", show_alert=True)
@dp.callback_query_handler(text="value4")
async def naj_value4(call: types.CallbackQuery):
    await call.answer(text="Эх блин , почти получилось, но у тебя почти не осталось зубов", show_alert=True)

@dp.callback_query_handler(text="value3")
async def naj_value3(call: types.CallbackQuery):
    await call.message.answer("Вы осмотрелись вокруг и ничего не нашли")
    await call.message.answer("В карманах тоже ничего")
    await call.message.answer("хотя нет, что это в ботинке?")
    await call.message.answer("Вы находите шпильку в ботинке")
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Вскрыть замок"]
    keyboard.add(*buttons)
    await call.message.answer("Может попробовать вскрыть замок?", reply_markup=keyboard)
    await call.answer()

@dp.message_handler(lambda message: message.text == "Вскрыть замок")
async def vsc_zamok(message: types.Message):
    await message.reply("Сыграем в мини-игру", reply_markup=types.ReplyKeyboardRemove())
    await asyncio.sleep(1)
    await message.answer("Для того чтобы вскрыть замок вы должны вводить степени двойки, пока замок не откроется")
    await asyncio.sleep(1)
    await message.answer("Начинаем с 2^1")

@dp.message_handler(lambda message: message.text == "2" or  message.text == "4" or  message.text == "8" or  message.text == "16" or  message.text == "32")
async def nepr_chislo(message: types.Message):
    await message.answer("Вы не вскрыли замок , думай дальше")

@dp.message_handler(lambda message: message.text == "64")
async def pr_chislo(message: types.Message):
    await message.answer("Поздравляю , вы вскрыли замок!")
    await asyncio.sleep(1)
    await message.answer("В коридоре послышались тяжелые , медленные шаги , направленные в левую сторону")
    await asyncio.sleep(1)
    await message.answer("Вы освободились и встали на ноги")
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="Попробовать открыть дверь", callback_data="value5"))
    keyboard.add(types.InlineKeyboardButton(text="Посмотреть в замочную скважину", callback_data="value6"))
    keyboard.add(types.InlineKeyboardButton(text="Взять револьвер", callback_data="value7"))
    await message.answer("Выберите ваше дальнейшее действие", reply_markup=keyboard)

@dp.callback_query_handler(text="value5")
async def naj_value2(call: types.CallbackQuery):
    await call.answer(text="Дверь закрыта", show_alert=True)
@dp.callback_query_handler(text="value6")
async def naj_value4(call: types.CallbackQuery):
    await call.answer(text="Вы увидели пустой , темный, узкий коридор, вы заметили ,что с справа чуть светлее", show_alert=True)

@dp.callback_query_handler(text="value7")
async def naj_value3(call: types.CallbackQuery):
    await call.message.answer("Вы взяли револьвер и проверили барабан , там 1 патрон")
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Попробовать прострелить замок", "Испугаться и остаться на месте"]
    keyboard.add(*buttons)
    await call.message.answer("Ваши дальнейшие действия?", reply_markup=keyboard)
    await call.answer()

@dp.message_handler(lambda message: message.text == "Попробовать прострелить замок")
async def prstr_zamok(message: types.Message):
    await message.reply("Вы прострелили замок", reply_markup=types.ReplyKeyboardRemove())
    await message.answer("Вы выходите в коридор и слышите шум слева")
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Побежать направо", "Побежать налево", "Остаться на месте"]
    keyboard.add(*buttons)
    await message.answer("Ваши дальнейшие действия?", reply_markup=keyboard)

@dp.message_handler(lambda message: message.text == "Испугаться и остаться на месте" or message.text == "Побежать налево" or message.text == "Остаться на месте")
async def proigr(message: types.Message):
    await message.reply("До вас добрались", reply_markup=types.ReplyKeyboardRemove())
    await asyncio.sleep(1)
    await message.answer("и снова заперли в комнате на цепи")
    await asyncio.sleep(1)
    await message.answer("Вы проиграли :(")
    await message.answer("😔")

@dp.message_handler(lambda message: message.text == "Побежать направо")
async def pobeda(message: types.Message):
    await message.reply("Вы со всей силы рванули направо", reply_markup=types.ReplyKeyboardRemove())
    await asyncio.sleep(1)
    await message.answer("Вы слышите , как за вами бегут , и вот вот догонят")
    await asyncio.sleep(1)
    await message.answer("Вы видите впереди запертую дверь, долго думаете и решаете попробовать выбить")
    await asyncio.sleep(1)
    await message.answer("Вы выбили дверь и выбрались наружу")
    await asyncio.sleep(1)
    await message.answer("Вы выиграли!")
    await message.answer_dice(emoji="🎯")

if __name__ == "__main__":
    # Запуск бота
    executor.start_polling(dp, skip_updates=True)

